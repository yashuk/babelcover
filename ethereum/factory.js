import web3 from './web3';
import InsuranceFactory from './Build/InsuranceFactory.json';

const instance = new web3.eth.Contract(
  JSON.parse(InsuranceFactory.interface),
  '0xE0b0a65A5E1E9175a24dcef210752Fcd78B2c0D0'
);

export default instance;
