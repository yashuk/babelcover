import React, { Component } from 'react';
import { Form, Button, Input, Message } from 'semantic-ui-react';
import Layout from '../../components/Layout';
import factory from '../../ethereum/factory';
import web3 from '../../ethereum/web3';
import { Router } from '../../routes';

class InsuranceNew extends Component {
  state = {
    premium: 0,
    catastropheFee: 0,
    duration: 0,
    errorMessage: '',
    loading: false
  };

  onSubmit = async event => {
    event.preventDefault();

    this.setState({ loading: true, errorMessage: '' });

    try {
      const accounts = await web3.eth.getAccounts();
      await factory.methods
        .createInsurance(1, 1, this.state.catastropheFee, this.state.premium, this.state.duration)
        .send({
          from: accounts[0]
        });

      Router.pushRoute('/');
    } catch (err) {
      this.setState({ errorMessage: err.message });
    }

    this.setState({ loading: false });
  };

  render() {
    return (
      <Layout>
        <h3>Create a Insurance</h3>
        <Form onSubmit={this.onSubmit} error={!!this.state.errorMessage}>
          <Form.Field>
            <label>Premium</label>
            <Input
              label="wei"
              labelPosition="right"
              value={this.state.premium}
              onChange={event =>
                this.setState({ premium: event.target.value })}
            />
          </Form.Field>

          <Form.Field>
            <label>Catastrophe Fee</label>
            <Input
              label="wei"
              labelPosition="right"
              value={this.state.catastropheFee}
              onChange={event =>
                this.setState({ catastropheFee: event.target.value })}
            />
          </Form.Field>

          <Form.Field>
            <label>Duration</label>
            <Input
              label="Years"
              labelPosition="right"
              value={this.state.duration}
              onChange={event =>
                this.setState({ duration: event.target.value })}
            />
          </Form.Field>

          <Message error header="Oops!" content={this.state.errorMessage} />
          <Button loading={this.state.loading} primary>
            Create!
          </Button>
        </Form>
      </Layout>
    );
  }
}

export default InsuranceNew;
