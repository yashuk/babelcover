import React, { Component } from 'react';
import { Card, Button } from 'semantic-ui-react';
import factory from '../ethereum/factory';
import Layout from '../components/Layout';
import { Link } from '../routes';

class InsuranceIndex extends Component {
  static async getInitialProps() {
    const insurances = await factory.methods.getUserInsurances(1).call();

    return { insurances };
  }

  renderInsurances() {
    const items = this.props.insurances.map(address => {
      return {
        header: address,
        description: (
          <Link route={`/insurance/${address}`}>
            <a>View Insurance</a>
          </Link>
        ),
        fluid: true
      };
    });

    return <Card.Group items={items} />;
  }

  render() {
    return (
      <Layout>
        <div>
          <h3>Open insurances</h3>

          <Link route="/insurance/new">
            <a>
              <Button
                floated="right"
                content="Create Insurance"
                icon="add circle"
                primary
              />
            </a>
          </Link>

          {this.renderInsurances()}
        </div>
      </Layout>
    );
  }
}

export default InsuranceIndex;
